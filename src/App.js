import React, { Component } from 'react';
import LoginTemplate from './components/login-template';
import Template from './components/template'

class App extends Component {
  render() {
    return (
      <div>
        {/* <LoginTemplate/> */}
        <Template/>
      </div>
    );
  }
}

export default App;
