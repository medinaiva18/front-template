import React, { Component } from 'react';

import Container from './container';
import SideBar from './side-bar';

import './template.css';
export default class Template extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            showMenu:false
        }
    }

    render() {
        return (
            <div className="wrapper">
                <SideBar/>
                {/* <div style={{width: "100%", height: "100%", margin: "0"}}>
                <LateralMenu show={this.state.showMenu} />
                <Container openMenu={this.handleOpenMenu.bind(this)} show={this.state.showMenu}/>
                </div> */}
            </div>
        );
    }

    handleOpenMenu(e){
        e.preventDefault();
        this.setState({
            showMenu: !this.state.showMenu
        });
    }
}