import React, {Component} from 'react';
import {MdClose, MdSettings, MdPowerSettingsNew} from 'react-icons/md';

export default class SideBar extends Component {
    constructor(props, context){
        super(props);
        this.state={
            classApply:"collapsed"
        };
    }

    render() {
        return (
            <div className="sidebar">
                <div className="sidebar-wrapper">
                    <div className="logo">
                        <a href="#">Logo</a>
                    </div>
                    <ul className="nav">
                        <li>
                            <a></a>
                        </li>
                    </ul>

                </div>
                {/* <div id="sidebar" className={this.props.show ? "" : this.state.classApply}>
                <ul>
                    <li><a href="#"><MdClose/></a></li>
                    <li><a href="#"><MdSettings/></a></li>
                    <li><a href="#"><MdPowerSettingsNew/></a></li>
                </ul>
                </div> */}
            </div>
        );
    }

}