import React, { Component } from 'react';
import Login from '../login'
import {FaProjectDiagram} from 'react-icons/fa';

import {
    ButtonToolbar, Button,
    Overlay, Popover,
    Navbar, Nav,
    Form
} from 'react-bootstrap';

import './header.css'

export default class Header extends Component {
    constructor(props, context) {
        super(props, context);

        this.handleClick = ({ target }) => {
            this.setState(s => ({ target, showLogin: !s.showLogin }));
        };
        
        this.state = {
            showLogin: false
        }
    }
    render() {
        const popover = (
                        <Popover>
                            <Login showSignUpEvent = {this.props.signUpChange}
                                endSessionView  = {() => {this.setState({showLogin: !this.state.showLogin})}}/>
                        </Popover>);
        return (
            <header>
                <Navbar bg="dark" variant="dark">
                    <Nav className="mr-auto">
                        <Nav.Link href="#home"><FaProjectDiagram className="image"/></Nav.Link>
                    </Nav>
                    <Form inline>
                        <ButtonToolbar className="position">
                            <Button onClick={this.handleClick} variant="outline-primary" className="position">Iniciar Sesion</Button>
                            <Overlay show={this.state.showLogin}
                                target={this.state.target}
                                placement="bottom">
                                {popover}
                            </Overlay>
                        </ButtonToolbar>
                        <Button variant="outline-primary" className="position">Acerca de</Button>
                    </Form>
                </Navbar>
            </header>
        );
    }

}