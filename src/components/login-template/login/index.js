/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import {Button} from 'react-bootstrap';
import {FaUserCircle} from 'react-icons/fa';

import './login.css'
export default class Login extends Component {

    constructor(props, context) {
        super(props);
        this.state = {
            name: '',
            password: ''
        };
    }

    render() {
        return (
            <div>
                <div className="fadeIn first text-center">
                    <h3 className="user-icon-h5"><FaUserCircle className="user-icon"/></h3>
                </div>
                <form>
                    <input type="text" className="login-input fadeIn second" placeholder="nombre"/>
                    <input type="text" className="login-input fadeIn third" placeholder="contraseña"/>
                    <Button  className="fadeIn int">Iniciar</Button>
                </form>
                <div className="formFooter">
                    <a onClick={this.handleShowSignUp.bind(this)} className="register-style-a underlineHover" href="#">Registrarse</a>
                </div>
            </div>
        );
    }

    handleShowSignUp(e){
        e.preventDefault();
        this.props.showSignUpEvent();
        this.props.endSessionView();
    }
}