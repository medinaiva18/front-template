import React, {Component} from 'react';
import {Carousel, Col, Card} from 'react-bootstrap';
import {FaFacebook, FaTwitter} from 'react-icons/fa';
import { IoIosCheckmarkCircleOutline } from "react-icons/io";
import SingUp from '../sign-up';

import './body-style.css'

export default class BodyLogin extends Component {
    constructor(props, context){
        super(props);
    }

    render(){
        const direction =  [
            require("../../imgs/login-carousel/i1.jpg"),
            require("../../imgs/login-carousel/i2.jpg"),
            require('../../imgs/login-carousel/i3.png'),
            require('../../imgs/login-carousel/i4.jpg'),
            require('../../imgs/login-carousel/i5.png')
        ];

        const descriptionItem = [
            "Maneja tus proyectos y tareas con facilidad",
            "Planifica visualmente en un cronograma",
            "Centraliza toda la comunicación en un sólo lugar",
            "Planificación ágil",
            "Estimación de proyectos",
            "Creación de informes ágiles"
        ];

        var items = (descriptionItem.map((v,k) => 
                 <div key={k} style={{display:'inline-flex'}}>
                    <div><IoIosCheckmarkCircleOutline className="card-item"/></div>
                    <div className="card-description">
                        <span>{v}</span>
                    </div>
                </div>
        ));

        var optionCard = (
            <Card className="conten-img-style" style={{ width: '100%', height:'100%'}}>
                <Card.Body>
                    <Card.Title>Gestione mejor sus proyectos</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">
                    La gestión de proyectos es mejor al ser visual.
                    Entiende en qué está trabajando tu equipo de un sólo vistazo.
                    </Card.Subtitle>
                    <Card.Text>
                        <div style={{display:'grid', marginTop: '1.2em'}}>{items}</div>
                    </Card.Text>
                    <Card.Link href="#"><FaFacebook/> Facebook</Card.Link>
                    <Card.Link href="#"><FaTwitter/> Twitter</Card.Link>
                </Card.Body>
            </Card>
        );

        var optionSingUp = (<div className="form-content" style={{display:'grid'}}><SingUp signUpClose={this.props.signUpClose}/></div>);

        var selectedOption = this.props.signUp ? (optionSingUp) : (optionCard);
        
        return(
            <div style={{display:'flex', paddingTop:'1em'}}>
                <Col md={6}>
                    {selectedOption}
                </Col>
                <Col md={6}>
                    <aside className="conten-img-style">
                        <Carousel interval={1000} fade={true}>
                            {direction.map((v, k) => 
                                <Carousel.Item key={k}>
                                    <img className="image-style" src={v}
                                    alt={k}/>
                                </Carousel.Item>)}
                        </Carousel>
                    </aside>
                </Col>
            </div>
        );
    }

}