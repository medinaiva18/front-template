import React, { Component } from 'react';
import Header from './header';
import BodyLogin from './body';

export default class LoginTemplate extends Component {
   
    constructor(props, context){
        super(props);
        this.state = {
            showSignUp: false
        }
    }

    render(){
        return(
            <div>
                <Header signUpChange={this.handleShowSignUp.bind(this)}/>
                <BodyLogin signUp={this.state.showSignUp} signUpClose={this.handleShowSignUp.bind(this)}/>
            </div>
        );
    }

    handleShowSignUp(e){
        this.setState({
            showSignUp: !this.state.showSignUp
        });
    }
}