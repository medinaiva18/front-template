import React , {Component} from 'react';
import {Form, Col, Row, Button} from 'react-bootstrap';
import {FaRegPaperPlane} from 'react-icons/fa';
import {MdClose} from 'react-icons/md';

import './sing-up-style.css';

export default class SingUp extends Component{
    
    constructor(props, context){
        super(props);
    }

    render(){
        return(
            <div>
                <span onClick={this.props.signUpClose} className="place-a"><MdClose className="icon-close"/></span>
                <Form>
                    <Form.Row style={{paddingLeft:'2em',marginTop:'1em'}}>
                        {/* <Col md={3}>
                            <div className="styled-input">
                                <input className="input-tam" type="text" required />
                                <label>Nombre</label>
                                <span></span>
                            </div>
                        </Col>*/}
                        <Col md={7}>
                            <label for="firs-name" className="inp">
                                <input type="text" id="firs-name" placeholder="&nbsp;"/>
                                <span className="label">Nombre</span>
                                <span className="border"></span>
                            </label>
                        </Col>
                        <Col md={5}>
                            <label for="last-name" className="inp">
                                <input type="text" id="last-name" placeholder="&nbsp;"/>
                                <span className="label">Apellido</span>
                                <span className="border"></span>
                            </label>
                        </Col>
                    </Form.Row>
                    <Form.Group style={{paddingLeft:'2em', marginTop:'1em'}} as={Row} controlId="formPlaintextEmail">
                        <Col sm="10">
                            <label for="input-email" className="inp">
                                <input type="email" id="input-email" placeholder="&nbsp;"/>
                                <span className="label">Correo</span>
                                <span className="border"></span>
                            </label>
                        </Col>
                    </Form.Group>
                    <Form.Group style={{paddingLeft:'2em', marginTop:'1em'}} as={Row} controlId="formPlaintextPassword">
                        <Col sm="10">
                            <label for="input-password" className="inp">
                                <input type="password" id="input-password" placeholder="&nbsp;"/>
                                <span className="label">Contraseña</span>
                                <span className="border"></span>
                            </label>
                        </Col>
                    </Form.Group>
                    <Form.Group style={{paddingLeft:'2em', marginTop:'1em'}} as={Row} controlId="formPlaintextPassword">
                        <Col sm="10">
                            <label for="input-repeat" className="inp">
                                <input type="password" id="input-repeat" placeholder="&nbsp;"/>
                                <span className="label">Repita Contraseña</span>
                                <span className="border"></span>
                            </label>
                        </Col>
                    </Form.Group>
                    <Form.Group style={{paddingLeft:'2em', marginTop:'1em'}}>
                        <Form.Check required label="Acepto los terminos y condiciones"/>
                    </Form.Group>
                    <Form.Group style={{paddingLeft:'2em', marginTop:'1em'}}>
                        <Button type="submit">Crear cuenta <FaRegPaperPlane/></Button>
                    </Form.Group>
                </Form>
            </div>
        );
    }
}
